
var fs = require('fs');
var steam = require('steam');
var irc = require('irc');
var _ = require('underscore');
var settings = require('./settings');
var http = require('http');

var firstMessage = "choc2!! I probably don't work right.";

exports.choc2 = choc2;
function choc2() {
	var self = this;

	self.info = {
		version: "1.0.0",
		author: "Megadrive",
		repo: "https://bitbucket.org/megadrive/choc2"
	};

	self.settings = {};

	self.steam = undefined;
	self.irc = undefined;

	self.http = new http.createServer();

	this.init = function(){
		console.log("choc2: version " + self.info.version);

		self.settings = settings.load();

		self.http.on('request', function(request, response){
			var pageContent = '';

			response.writeHead(200, {"Content-Type": "text/html"});

			pageContent = "<pre>Steam <-> IRC\n\n";
			pageContent += "Status:\n";
			pageContent += "\tSteam: broken\n";
			pageContent += "\tIRC: broken\n";

			response.end(pageContent);
		});

		self.http.listen(process.env.PORT || 80);
		self.http.on('error', function(e){
			console.log('server error:', e);
		});
	};

	this.connect = function(){
		console.log("choc2: connecting")

		self.joinIrc();
		self.joinSteam();

		if (fs.existsSync('servers')) {
			steam.servers = JSON.parse(fs.readFileSync('servers'));
		}
		else{
			console.error('SERROR: Steam couldnt find servers file.');
			process.exit(1); // exit because only one Steam server is known to work.
		}

		console.log(" ");
	};

	this.joinIrc = function(){
		console.log("       joining irc");

		self.irc = new irc.Client(self.settings.irc.server, self.settings.irc.nick, {
			'userName': 'choc2',
			'realName': 'choc2, for ausgaf <3',
			'port': 6667,
			'debug': false,
			'showErrors': false,
			'autoRejoin': true,
			'autoConnect': true,
			'channels': self.settings.irc.channels,
			'secure': false,
			'selfSigned': false,
			'certExpired': false,
			'floodProtection': false,
			'floodProtectionDelay': 1000,
			'sasl': false,
			'stripColors': false,
			'channelPrefixes': "&#",
			'messageSplit': 512
		});

		// NOTE: Need this to prevent crash
		self.irc.on('error', function(e){
			console.error('IERROR: ' + e);
		});

		self.irc.on('join' + self.settings.irc.output_channel, function(){
			self.irc.say(self.settings.irc.output_channel, firstMessage);
		});

		self.irc.on('message' + self.settings.irc.output_channel, function(nick, text, message){
			if( self.steam.loggedOn ){
				self.steam.sendMessage(self.settings.steam.chatRoomId, nick + ": " + text);
				self.doCommand(text);
			}
		});
	};

	this.joinSteam = function(){
		console.log("       joining steam");

		self.steam = new steam.SteamClient();

		self.steam.logOn({
			'accountName': self.settings.steam.username,
			'password': self.settings.steam.password
		});

		self.steam.on('loggedOn', function(){
			console.log("       connected to Steam");

			// NOTE: Let's play TF2 for funsies!
			self.steam.gamesPlayed([440]);

			self.steam.setPersonaName(self.settings.steam.nick);
			self.steam.setPersonaState(steam.EPersonaState.Online);
			self.steam.joinChat(self.settings.steam.chatRoomId);

			// TEST
			self.steam.sendMessage(self.settings.steam.chatRoomId, firstMessage);
		});

		self.steam.on('loggedOff', function(){
			console.log("       disconnected from Steam");
		});

		self.steam.on('chatMsg', function(chatRoomId, msg, EChatEntryType, steamId){
			var nick = self.steam.users[steamId].playerName;
			self.irc.say(self.settings.irc.output_channel, nick + ": " + msg);
			self.doCommand(msg);
		});

		self.steam.on('user', function(user){
			// make sure the user is in the chat room
			if( self.steam.chatRooms[self.settings.steam.chatRoomId] && self.steam.chatRooms[self.settings.steam.chatRoomId][user.friendid] !== undefined ){
				console.log("SUser");

				// Only output if it isn't the same game and if they started playing a game.
				if( user.gameName.length > 0 && user.gameName !== self.steam.users[user.friendid].gameName ){
					self.irc.say(self.settings.irc.output_channel, user.playerName + " has started playing " + user.gameName + "!");
				}
			}
		});

		self.steam.on('friend', function(steamId, relationship){
			console.log("SNewFriend");
			switch(relationship){
				case steam.EFriendRelationship.PendingInvitee:
					self.steam.addFriend(steamId);
					break;
			};
		});

		self.steam.on('chatStateChange', function(EChatMemberStateChange, steamId, chatRoomId, actorId){
			console.log("SChatStateChange");

			var chan = self.settings.irc.output_channel;

			if( EChatMemberStateChange == steam.EChatMemberStateChange.Kicked ||
				EChatMemberStateChange == steam.EChatMemberStateChange.Disconnected || 
				EChatMemberStateChange == steam.EChatMemberStateChange.Left ){
				self.steam.joinChat(self.settings.steam.chatRoomId); // auto rejoin
			}

			switch( EChatMemberStateChange ){
				case steam.EChatMemberStateChange.Entered:
					self.irc.say(chan, self.getSteamName(steamId) + " has joined.");
					break;
				case steam.EChatMemberStateChange.Left:
				case steam.EChatMemberStateChange.Disconnected:
					self.irc.say(chan, self.getSteamName(steamId) + " has left.");
					break;
				case steam.EChatMemberStateChange.Kicked:
					self.irc.say(chan, self.getSteamName(steamId) + " was kicked by " + self.getSteamName(actorId) + ".");
					break;
				case steam.EChatMemberStateChange.Banned:
					self.irc.say(chan, self.getSteamName(steamId) + " was banned by " + self.getSteamName(actorId) + ".");
					break;
			}
		});

		self.steam.on('error', function(e){
			console.error('SERROR: ' + e);
		});
	};

	this.logged = function(p){
		var rv = false;

		if( p === 'steam' ){
			rv = self.steam.loggedOn;
		}

		if( p === 'irc' ){
			rv = self.irc.chans.length > 0 ? true : false;
		}

		return rv;
	};

	this.getSteamName = function(steamId){
		var rv = '';
		if( self.steam.users[steamId] ){
			rv = self.steam.users[steamId].playerName;
		}
		return rv;
	};

	// NOTE: change this shit later
	this.doCommand = function(message){
/*
		var cmds = {
			'steam': function(){
				var nicks = self.irc.chans[self.settings.irc.output_channel].join(', ');
				self.irc.say(self.settings.irc.output_channel, "IRC: " + nicks);
			},
			'irc': function(){
				var nicks = [];
				for( var nick in self.steam.chatRooms[self.settings.steam.chatRoomId] ){
					nicks.push(nick);
				}
				self.steam.sendMessage(self.steam.chatRoomId, "Steam: " + nicks.join(', '));
			}
		};

		if( message.indexOf(0) === self.settings.commandDelimeter ){
			var msgCmd = message.split(' ', 1);

			for( var cmd in cmds ){
				console.log(cmd + " ::: " + msgCmd);
				if( commandDelimeter + cmd === msgCmd[0] ){
					cmds[cmd](); // idk
				}
			}
		}
*/
	};
};
